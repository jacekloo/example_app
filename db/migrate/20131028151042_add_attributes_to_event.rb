class AddAttributesToEvent < ActiveRecord::Migration
  def change
    add_column :events, :owner_id, :integer
    add_column :events, :facebook_id, :string
  end
end
