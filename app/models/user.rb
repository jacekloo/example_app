class User < ActiveRecord::Base

  devise :omniauthable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :name, :presence => true
  has_many :adminable_events, :class_name => 'Event', :foreign_key => :owner_id
  has_many :adminable_ads, :class_name => 'Ad', :foreign_key => :owner_id


  def social_graph
    if provider == "facebook" && access_token.present?
      FbGraph::User.me(access_token).fetch
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.create(name:auth.extra.raw_info.name,
                         provider:auth.provider,
                         uid:auth.uid,
                         email:auth.info.email,
                         password:Devise.friendly_token[0,20]
      )
    end
    user
  end

end